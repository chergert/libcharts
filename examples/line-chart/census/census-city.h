#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define CENSUS_TYPE_CITY (census_city_get_type())

G_DECLARE_FINAL_TYPE (CensusCity, census_city, CENSUS, CITY, GObject)

G_END_DECLS
