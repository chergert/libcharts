#include "census-city.h"

struct _CensusCity
{
  GObject parent_instance;
  GDateTime *sampled_at;
  guint count;
};

enum {
  PROP_0,
  PROP_COUNT,
  PROP_SAMPLED_AT,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (CensusCity, census_city, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static void
census_city_finalize (GObject *object)
{
  CensusCity *self = (CensusCity *)object;

  g_clear_pointer (&self->sampled_at, g_date_time_unref);

  G_OBJECT_CLASS (census_city_parent_class)->finalize (object);
}

static void
census_city_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  CensusCity *self = CENSUS_CITY (object);

  switch (prop_id)
    {
    case PROP_SAMPLED_AT:
      g_value_set_boxed (value, self->sampled_at);
      break;

    case PROP_COUNT:
      g_value_set_uint (value, self->count);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
census_city_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  CensusCity *self = CENSUS_CITY (object);

  switch (prop_id)
    {
    case PROP_SAMPLED_AT:
      self->sampled_at = g_value_dup_boxed (value);
      break;

    case PROP_COUNT:
      self->count = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
census_city_class_init (CensusCityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = census_city_finalize;
  object_class->get_property = census_city_get_property;
  object_class->set_property = census_city_set_property;

  properties [PROP_COUNT] =
    g_param_spec_uint ("count", NULL, NULL,
                       0, G_MAXUINT, 0,
                       (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SAMPLED_AT] =
    g_param_spec_boxed ("sampled-at", NULL, NULL,
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
census_city_init (CensusCity *self)
{
}
