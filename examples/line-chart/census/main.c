#include <libcharts.h>

#include "census-city.h"

int
main (int   argc,
      char *argv[])
{
  GtkBuilder *builder;
  GMainLoop *main_loop;
  GtkWindow *window;

  gtk_init ();

  g_type_ensure (CENSUS_TYPE_CITY);

  main_loop = g_main_loop_new (NULL, FALSE);
  builder = gtk_builder_new_from_resource ("/census.ui");
  window = GTK_WINDOW (gtk_builder_get_object (builder, "window"));
  g_signal_connect_swapped (window,
                            "close-request",
                            G_CALLBACK (g_main_loop_quit),
                            main_loop);
  gtk_window_present (window);
  g_main_loop_run (main_loop);

  return 0;
}
