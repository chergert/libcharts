# Libcharts

A library for creating charts with GTK 4.

This is a playground for using `GskPath` and therefore requires specific
branches of GTK.  However, for the time being I'm using Cairo so that I can
possibly use this from Sysprof until the GskPath bits are ready for GTK 4.

Longer term I want to make a bunch of things interactive, such as data
points, using actual GtkWidgets for input. This will be improved a bunch
once we can do hit-targetting with GskPath, so again, I'm going to hold
out on that for the time being.
