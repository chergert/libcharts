/*
 * charts-spline-chart.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-normalized-series.h"
#include "charts-spline-chart.h"
#include "charts-spline-series.h"

G_DEFINE_TYPE (ChartsSplineChart, charts_spline_chart, CHARTS_TYPE_XY_CHART)

/**
 * charts_spline_chart_new:
 *
 * Create a new #ChartsSplineChart.
 *
 * Returns: (transfer full): a newly created #ChartsSplineChart
 */
GtkWidget *
charts_spline_chart_new (void)
{
  return g_object_new (CHARTS_TYPE_SPLINE_CHART, NULL);
}

static void
charts_spline_chart_snapshot_xy (ChartsXYChart          *xy_chart,
                                 GtkSnapshot            *snapshot,
                                 ChartsNormalizedSeries *x_normalized,
                                 ChartsNormalizedSeries *y_normalized)
{
  ChartsSplineChart *self = (ChartsSplineChart *)xy_chart;
  const GdkRGBA *color;
  ChartsSeries *series;
  graphene_rect_t area;
  const float *x_values;
  const float *y_values;
  float last_x;
  float last_y;
  cairo_t *cr;
  guint n_x_values;
  guint n_y_values;
  guint n_values;

  g_assert (CHARTS_IS_SPLINE_CHART (self));
  g_assert (GTK_IS_SNAPSHOT (snapshot));
  g_assert (CHARTS_IS_NORMALIZED_SERIES (x_normalized));
  g_assert (CHARTS_IS_NORMALIZED_SERIES (y_normalized));

  x_values = charts_normalized_series_get_values (x_normalized, &n_x_values);
  y_values = charts_normalized_series_get_values (y_normalized, &n_y_values);
  n_values = MIN (n_x_values, n_y_values);

  if (n_values == 0)
    return;

  area.origin.x = 0;
  area.origin.y = 0;
  area.size.width = gtk_widget_get_width (GTK_WIDGET (self));
  area.size.height = gtk_widget_get_height (GTK_WIDGET (self));

  if (area.size.width == 0 || area.size.height == 0)
    return;

  cr = gtk_snapshot_append_cairo (snapshot, &area);

  cairo_set_matrix (cr, &(cairo_matrix_t) {1, 0, 0, -1, 0, area.size.height});

  last_x = x_values[0] * area.size.width;
  last_y = y_values[0] * area.size.height;

  cairo_move_to (cr, last_x, last_y);

  for (guint i = 1; i < n_values; i++)
    {
      float x = x_values[i] * area.size.width;
      float y = y_values[i] * area.size.height;

      cairo_curve_to (cr,
                      last_x + ((x - last_x)/2),
                      last_y,
                      last_x + ((x - last_x)/2),
                      y,
                      x,
                      y);

      last_x = x;
      last_y = y;
    }

  series = charts_normalized_series_get_series (x_normalized);
  color = charts_xy_series_get_color (CHARTS_XY_SERIES (series));

  gdk_cairo_set_source_rgba (cr, color);
  cairo_stroke (cr);

  cairo_destroy (cr);
}

static void
charts_spline_chart_class_init (ChartsSplineChartClass *klass)
{
  ChartsXYChartClass *xy_chart_class = CHARTS_XY_CHART_CLASS (klass);

  xy_chart_class->snapshot_xy = charts_spline_chart_snapshot_xy;
}

static void
charts_spline_chart_init (ChartsSplineChart *self)
{
}

