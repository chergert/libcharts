/*
 * charts-xy-chart.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-chart.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_XY_CHART         (charts_xy_chart_get_type())
#define CHARTS_IS_XY_CHART(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_XY_CHART))
#define CHARTS_XY_CHART(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_XY_CHART, ChartsXYChart))
#define CHARTS_XY_CHART_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_XY_CHART, ChartsXYChartClass))

typedef struct _ChartsXYChart ChartsXYChart;
typedef struct _ChartsXYChartClass ChartsXYChartClass;

CHARTS_AVAILABLE_IN_ALL
GType       charts_xy_chart_get_type   (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsAxis *charts_xy_chart_get_x_axis (ChartsXYChart *self);
CHARTS_AVAILABLE_IN_ALL
void        charts_xy_chart_set_x_axis (ChartsXYChart *self,
                                        ChartsAxis    *axis);
CHARTS_AVAILABLE_IN_ALL
ChartsAxis *charts_xy_chart_get_y_axis (ChartsXYChart *self);
CHARTS_AVAILABLE_IN_ALL
void        charts_xy_chart_set_y_axis (ChartsXYChart *self,
                                        ChartsAxis    *axis);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsXYChart, g_object_unref)

G_END_DECLS
