/* charts-spline-series.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-spline-series.h"

G_DEFINE_TYPE (ChartsSplineSeries, charts_spline_series, CHARTS_TYPE_XY_SERIES)

static void
charts_spline_series_class_init (ChartsSplineSeriesClass *klass)
{
}

static void
charts_spline_series_init (ChartsSplineSeries *self)
{
}

ChartsSplineSeries *
charts_spline_series_new (void)
{
  return g_object_new (CHARTS_TYPE_SPLINE_SERIES, NULL);
}

