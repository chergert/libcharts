/* charts-date-time-axis.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-date-time-axis.h"

enum {
  PROP_0,
  PROP_MIN_VALUE,
  PROP_MAX_VALUE,
  N_PROPS
};

G_DEFINE_TYPE (ChartsDateTimeAxis, charts_date_time_axis, CHARTS_TYPE_AXIS)

static GParamSpec *properties [N_PROPS];
static GType date_time_type;

static void
charts_date_time_axis_update (ChartsDateTimeAxis *self)
{
  g_assert (CHARTS_IS_DATE_TIME_AXIS (self));

  self->min_value_unix = self->min_value ? g_date_time_to_unix (self->min_value) : 0;
  self->max_value_unix = self->max_value ? g_date_time_to_unix (self->max_value) : 0;
  self->distance_unix = self->max_value_unix - self->min_value_unix;

  _charts_axis_emit_range_changed (CHARTS_AXIS (self));
}

static void
charts_date_time_axis_real_get_min_value (ChartsAxis *axis,
                                          GValue     *value)
{
  ChartsDateTimeAxis *self = CHARTS_DATE_TIME_AXIS (axis);
  gint64 min_value = G_MININT64;
  gint64 max_value = G_MAXINT64;

  g_value_init (value, G_TYPE_DATE_TIME);

  if (self->min_value)
    min_value = g_date_time_to_unix (self->min_value);

  if (self->max_value)
    max_value = g_date_time_to_unix (self->max_value);

  if (min_value < max_value)
    g_value_set_boxed (value, self->min_value);
  else
    g_value_set_boxed (value, self->max_value);
}

static double
charts_date_time_axis_normalize (ChartsAxis   *axis,
                                 const GValue *value)
{
  ChartsDateTimeAxis *self = (ChartsDateTimeAxis *)axis;
  GDateTime *dt;

  if (G_VALUE_HOLDS (value, date_time_type) && (dt = g_value_get_boxed (value)))
    {
      gint64 v = g_date_time_to_unix (dt);
      return (v - self->min_value_unix) / (double)self->distance_unix;
    }

  return .0;
}

static void
charts_date_time_axis_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  ChartsDateTimeAxis *self = CHARTS_DATE_TIME_AXIS (object);

  switch (prop_id)
    {
    case PROP_MIN_VALUE:
      g_value_set_boxed (value, charts_date_time_axis_get_min_value (self));
      break;

    case PROP_MAX_VALUE:
      g_value_set_boxed (value, charts_date_time_axis_get_max_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_date_time_axis_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  ChartsDateTimeAxis *self = CHARTS_DATE_TIME_AXIS (object);

  switch (prop_id)
    {
    case PROP_MIN_VALUE:
      charts_date_time_axis_set_min_value (self, g_value_get_boxed (value));
      break;

    case PROP_MAX_VALUE:
      charts_date_time_axis_set_max_value (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_date_time_axis_class_init (ChartsDateTimeAxisClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ChartsAxisClass *axis_class = CHARTS_AXIS_CLASS (klass);

  object_class->get_property = charts_date_time_axis_get_property;
  object_class->set_property = charts_date_time_axis_set_property;

  axis_class->get_min_value = charts_date_time_axis_real_get_min_value;
  axis_class->normalize = charts_date_time_axis_normalize;

  properties[PROP_MIN_VALUE] =
    g_param_spec_boxed ("min-value", NULL, NULL,
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_MAX_VALUE] =
    g_param_spec_boxed ("max-value", NULL, NULL,
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  date_time_type = G_TYPE_DATE_TIME;
}

static void
charts_date_time_axis_init (ChartsDateTimeAxis *self)
{
}

/**
 * charts_date_time_axis_get_min_value:
 * @self: a #ChartsDateTimeAxis
 *
 * Gets the minimum date time value.
 *
 * Returns: (transfer none) (nullable): a #GDateTime or %NULL
 */
GDateTime *
charts_date_time_axis_get_min_value (ChartsDateTimeAxis *self)
{
  g_return_val_if_fail (CHARTS_IS_DATE_TIME_AXIS (self), NULL);

  return self->min_value;
}

void
charts_date_time_axis_set_min_value (ChartsDateTimeAxis *self,
                                     GDateTime          *min_value)
{
  g_return_if_fail (CHARTS_IS_DATE_TIME_AXIS (self));

  if (min_value == self->min_value)
    return;

  if (min_value != NULL)
    g_date_time_ref (min_value);

  g_clear_pointer (&self->min_value , g_date_time_unref);
  self->min_value = min_value;

  charts_date_time_axis_update (self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MIN_VALUE]);
}

/**
 * charts_date_time_axis_get_max_value:
 * @self: a #ChartsDateTimeAxis
 *
 * Gets the maximum #GDateTime in the axis.
 *
 * Returns: (transfer none) (nullable): a #GDateTime or %NULL
 */
GDateTime *
charts_date_time_axis_get_max_value (ChartsDateTimeAxis *self)
{
  g_return_val_if_fail (CHARTS_IS_DATE_TIME_AXIS (self), NULL);

  return self->max_value;
}

void
charts_date_time_axis_set_max_value (ChartsDateTimeAxis *self,
                                     GDateTime          *max_value)
{
  g_return_if_fail (CHARTS_IS_DATE_TIME_AXIS (self));

  if (max_value == self->max_value)
    return;

  if (max_value != NULL)
    g_date_time_ref (max_value);

  g_clear_pointer (&self->max_value , g_date_time_unref);
  self->max_value = max_value;

  charts_date_time_axis_update (self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MAX_VALUE]);
}
