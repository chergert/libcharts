/* charts-spline-chart.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-xy-chart.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_SPLINE_CHART         (charts_spline_chart_get_type())
#define CHARTS_IS_SPLINE_CHART(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_SPLINE_CHART))
#define CHARTS_SPLINE_CHART(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_SPLINE_CHART, ChartsSplineChart))
#define CHARTS_SPLINE_CHART_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_SPLINE_CHART, ChartsSplineChartClass))

typedef struct _ChartsSplineChart ChartsSplineChart;
typedef struct _ChartsSplineChartClass ChartsSplineChartClass;

CHARTS_AVAILABLE_IN_ALL
GType      charts_spline_chart_get_type      (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
GtkWidget *charts_spline_chart_new           (void);
CHARTS_AVAILABLE_IN_ALL
void       charts_spline_chart_set_x_axis    (ChartsSplineChart   *self,
                                              ChartsAxis          *axis);
CHARTS_AVAILABLE_IN_ALL
void       charts_spline_chart_set_y_axis    (ChartsSplineChart   *self,
                                              ChartsAxis          *axis);
CHARTS_AVAILABLE_IN_ALL
void       charts_spline_chart_add_series    (ChartsSplineChart   *self,
                                              ChartsSplineSeries  *series);
CHARTS_AVAILABLE_IN_ALL
void       charts_spline_chart_remove_series (ChartsSplineChart   *self,
                                              ChartsSplineSeries  *series);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsSplineChart, g_object_unref)

G_END_DECLS

