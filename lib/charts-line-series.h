/* charts-line-series.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-types.h"
#include "charts-xy-series.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_LINE_SERIES         (charts_line_series_get_type())
#define CHARTS_IS_LINE_SERIES(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_LINE_SERIES))
#define CHARTS_LINE_SERIES(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_LINE_SERIES, ChartsLineSeries))
#define CHARTS_LINE_SERIES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_LINE_SERIES, ChartsLineSeriesClass))

typedef struct _ChartsLineSeries ChartsLineSeries;
typedef struct _ChartsLineSeriesClass ChartsLineSeriesClass;

CHARTS_AVAILABLE_IN_ALL
GType             charts_line_series_get_type       (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsLineSeries *charts_line_series_new            (void);
CHARTS_AVAILABLE_IN_ALL
double            charts_line_series_get_line_width (ChartsLineSeries *self);
CHARTS_AVAILABLE_IN_ALL
void              charts_line_series_set_line_width (ChartsLineSeries *self,
                                                     double            line_width);
CHARTS_AVAILABLE_IN_ALL
const GdkRGBA    *charts_line_series_get_fill_color (ChartsLineSeries *self);
CHARTS_AVAILABLE_IN_ALL
void              charts_line_series_set_fill_color (ChartsLineSeries *self,
                                                     const GdkRGBA    *fill_color);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsLineSeries, g_object_unref)

G_END_DECLS
