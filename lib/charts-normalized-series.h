/* charts-normalized-series.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "charts-types.h"
#include "charts-version-macros.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_NORMALIZED_SERIES         (charts_normalized_series_get_type())
#define CHARTS_IS_NORMALIZED_SERIES(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_NORMALIZED_SERIES))
#define CHARTS_NORMALIZED_SERIES(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_NORMALIZED_SERIES, ChartsNormalizedSeries))
#define CHARTS_NORMALIZED_SERIES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_NORMALIZED_SERIES, ChartsNormalizedSeriesClass))

typedef struct _ChartsNormalizedSeries ChartsNormalizedSeries;
typedef struct _ChartsNormalizedSeriesClass ChartsNormalizedSeriesClass;

CHARTS_AVAILABLE_IN_ALL
GType                   charts_normalized_series_get_type       (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsNormalizedSeries *charts_normalized_series_new            (ChartsSeries           *series,
                                                                 GtkExpression          *expression,
                                                                 ChartsAxis             *axis);
CHARTS_AVAILABLE_IN_ALL
GtkExpression          *charts_normalized_series_get_expression (ChartsNormalizedSeries *self);
CHARTS_AVAILABLE_IN_ALL
void                    charts_normalized_series_set_expression (ChartsNormalizedSeries *self,
                                                                 GtkExpression          *expression);
CHARTS_AVAILABLE_IN_ALL
ChartsSeries           *charts_normalized_series_get_series     (ChartsNormalizedSeries *self);
CHARTS_AVAILABLE_IN_ALL
void                    charts_normalized_series_set_series     (ChartsNormalizedSeries *self,
                                                                 ChartsSeries           *series);
CHARTS_AVAILABLE_IN_ALL
ChartsAxis             *charts_normalized_series_get_axis       (ChartsNormalizedSeries *self);
CHARTS_AVAILABLE_IN_ALL
void                    charts_normalized_series_set_axis       (ChartsNormalizedSeries *self,
                                                                 ChartsAxis             *axis);
CHARTS_AVAILABLE_IN_ALL
const float            *charts_normalized_series_get_values     (ChartsNormalizedSeries *self,
                                                                 guint                  *n_values);

G_END_DECLS
