/* libcharts.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gtk/gtk.h>

#define LIBCHARTS_INSIDE

# include "charts-types.h"
# include "charts-version-macros.h"
# include "charts-version.h"

#include "charts-init.h"

# include "charts-axis.h"
# include "charts-date-time-axis.h"
# include "charts-value-axis.h"

# include "charts-series.h"
# include "charts-xy-series.h"
# include "charts-line-series.h"
# include "charts-spline-series.h"
# include "charts-normalized-series.h"

# include "charts-chart.h"
# include "charts-line-chart.h"
# include "charts-spline-chart.h"

#undef LIBCHARTS_INSIDE
