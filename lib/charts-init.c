/*
 * charts-init.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-axis.h"
#include "charts-chart.h"
#include "charts-date-time-axis.h"
#include "charts-init.h"
#include "charts-line-chart.h"
#include "charts-line-series.h"
#include "charts-series.h"
#include "charts-spline-chart.h"
#include "charts-spline-series.h"
#include "charts-value-axis.h"
#include "charts-xy-chart.h"
#include "charts-xy-series.h"

#include "gconstructor.h"

void
charts_init (void)
{
  g_type_ensure (CHARTS_TYPE_AXIS);
  g_type_ensure (CHARTS_TYPE_CHART);
  g_type_ensure (CHARTS_TYPE_DATE_TIME_AXIS);
  g_type_ensure (CHARTS_TYPE_LINE_CHART);
  g_type_ensure (CHARTS_TYPE_LINE_SERIES);
  g_type_ensure (CHARTS_TYPE_SERIES);
  g_type_ensure (CHARTS_TYPE_SPLINE_CHART);
  g_type_ensure (CHARTS_TYPE_SPLINE_SERIES);
  g_type_ensure (CHARTS_TYPE_VALUE_AXIS);
  g_type_ensure (CHARTS_TYPE_XY_CHART);
  g_type_ensure (CHARTS_TYPE_XY_SERIES);
}

G_DEFINE_CONSTRUCTOR (_charts_init)

static void
_charts_init (void)
{
  charts_init ();
}
