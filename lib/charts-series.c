/* charts-series.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-series.h"

enum {
  PROP_0,
  PROP_MODEL,
  PROP_TITLE,
  N_PROPS
};

static GType
charts_series_get_item_type (GListModel *model)
{
  ChartsSeries *self = CHARTS_SERIES (model);

  if (self->model != NULL)
    return g_list_model_get_item_type (self->model);

  return G_TYPE_OBJECT;
}

static guint
charts_series_get_n_items (GListModel *model)
{
  ChartsSeries *self = CHARTS_SERIES (model);

  if (self->model != NULL)
    return g_list_model_get_n_items (self->model);

  return 0;
}

static gpointer
charts_series_get_item (GListModel *model,
                        guint       position)
{
  ChartsSeries *self = CHARTS_SERIES (model);

  if (self->model != NULL)
    return g_list_model_get_item (self->model, position);

  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = charts_series_get_item_type;
  iface->get_n_items = charts_series_get_n_items;
  iface->get_item = charts_series_get_item;
}

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (ChartsSeries, charts_series, G_TYPE_OBJECT,
                                  G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static GParamSpec *properties [N_PROPS];

static void
charts_series_real_items_changed (ChartsSeries *self,
                                  GListModel   *model,
                                  guint         position,
                                  guint         removed,
                                  guint         added)
{
  g_list_model_items_changed (G_LIST_MODEL (self), position, removed, added);
}

static void
charts_series_items_changed_cb (ChartsSeries *self,
                                guint         position,
                                guint         removed,
                                guint         added,
                                GListModel   *model)
{
  g_assert (CHARTS_IS_SERIES (self));
  g_assert (G_IS_LIST_MODEL (model));

  CHARTS_SERIES_GET_CLASS (self)->items_changed (self, model, position, removed, added);
}

static void
charts_series_dispose (GObject *object)
{
  ChartsSeries *self = (ChartsSeries *)object;

  charts_series_set_model (self, NULL);

  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (charts_series_parent_class)->dispose (object);
}

static void
charts_series_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ChartsSeries *self = CHARTS_SERIES (object);

  switch (prop_id)
    {
    case PROP_MODEL:
      g_value_set_object (value, charts_series_get_model (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, charts_series_get_title (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_series_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ChartsSeries *self = CHARTS_SERIES (object);

  switch (prop_id)
    {
    case PROP_MODEL:
      charts_series_set_model (self, g_value_get_object (value));
      break;

    case PROP_TITLE:
      charts_series_set_title (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_series_class_init (ChartsSeriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = charts_series_dispose;
  object_class->get_property = charts_series_get_property;
  object_class->set_property = charts_series_set_property;

  klass->items_changed = charts_series_real_items_changed;

  properties[PROP_MODEL] =
    g_param_spec_object ("model", NULL, NULL,
                         G_TYPE_LIST_MODEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
charts_series_init (ChartsSeries *self)
{
}

const char *
charts_series_get_title (ChartsSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_SERIES (self), NULL);

  return self->title;
}

void
charts_series_set_title (ChartsSeries *self,
                         const char   *title)
{
  g_return_if_fail (CHARTS_IS_SERIES (self));

  if (g_set_str (&self->title, title))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TITLE]);
}

/**
 * charts_series_get_model:
 * @self: a #ChartsSeries
 *
 * Returns: (transfer none) (nullable): a #GListModel
 */
GListModel *
charts_series_get_model (ChartsSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_SERIES (self), NULL);

  return self->model;
}

void
charts_series_set_model (ChartsSeries *self,
                         GListModel   *model)
{
  guint old_len = 0;
  guint new_len = 0;

  g_return_if_fail (CHARTS_IS_SERIES (self));
  g_return_if_fail (!model || G_IS_LIST_MODEL (model));

  if (model == self->model)
    return;

  if (model != NULL)
    {
      new_len = g_list_model_get_n_items (model);
      g_object_ref (model);
    }

  if (self->model != NULL)
    {
      old_len = g_list_model_get_n_items (self->model);
      g_clear_signal_handler (&self->items_changed_handler, self->model);
      CHARTS_SERIES_GET_CLASS (self)->items_changed (self, self->model, 0, old_len, 0);
    }

  g_set_object (&self->model, model);

  if (model != NULL)
    {
      self->items_changed_handler =
        g_signal_connect_object (model,
                                 "items-changed",
                                 G_CALLBACK (charts_series_items_changed_cb),
                                 self,
                                 G_CONNECT_SWAPPED);
      CHARTS_SERIES_GET_CLASS (self)->items_changed (self, self->model, 0, 0, new_len);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MODEL]);
}
