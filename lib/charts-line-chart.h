/* charts-line-chart.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-xy-chart.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_LINE_CHART         (charts_line_chart_get_type())
#define CHARTS_IS_LINE_CHART(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_LINE_CHART))
#define CHARTS_LINE_CHART(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_LINE_CHART, ChartsLineChart))
#define CHARTS_LINE_CHART_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_LINE_CHART, ChartsLineChartClass))

typedef struct _ChartsLineChart ChartsLineChart;
typedef struct _ChartsLineChartClass ChartsLineChartClass;

CHARTS_AVAILABLE_IN_ALL
GType      charts_line_chart_get_type      (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
GtkWidget *charts_line_chart_new           (void);
CHARTS_AVAILABLE_IN_ALL
void       charts_line_chart_add_series    (ChartsLineChart   *self,
                                            ChartsLineSeries  *series);
CHARTS_AVAILABLE_IN_ALL
void       charts_line_chart_remove_series (ChartsLineChart   *self,
                                            ChartsLineSeries  *series);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsLineChart, g_object_unref)

G_END_DECLS
