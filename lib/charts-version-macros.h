/* charts-version-macros.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <glib.h>

#include "charts-version.h"

G_BEGIN_DECLS

#ifndef _CHARTS_EXTERN
# define _CHARTS_EXTERN extern
#endif

#ifdef CHARTS_DISABLE_DEPRECATION_WARNINGS
# define CHARTS_DEPRECATED _CHARTS_EXTERN
# define CHARTS_DEPRECATED_FOR(f) _CHARTS_EXTERN
# define CHARTS_UNAVAILABLE(maj,min) _CHARTS_EXTERN
#else
# define CHARTS_DEPRECATED G_DEPRECATED _CHARTS_EXTERN
# define CHARTS_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _CHARTS_EXTERN
# define CHARTS_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _CHARTS_EXTERN
#endif

#define CHARTS_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (CHARTS_MINOR_VERSION == 99)
# define CHARTS_VERSION_CUR_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION + 1, 0))
#elif (CHARTS_MINOR_VERSION % 2)
# define CHARTS_VERSION_CUR_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION, CHARTS_MINOR_VERSION + 1))
#else
# define CHARTS_VERSION_CUR_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION, CHARTS_MINOR_VERSION))
#endif

#if (CHARTS_MINOR_VERSION == 99)
# define CHARTS_VERSION_PREV_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION + 1, 0))
#elif (CHARTS_MINOR_VERSION % 2)
# define CHARTS_VERSION_PREV_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION, CHARTS_MINOR_VERSION - 1))
#else
# define CHARTS_VERSION_PREV_STABLE (G_ENCODE_VERSION (CHARTS_MAJOR_VERSION, CHARTS_MINOR_VERSION - 2))
#endif

/**
 * CHARTS_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the libcharts.h header.
 *
 * The definition should be one of the predefined Drafting version
 * macros: %CHARTS_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Drafting API to use.
 *
 * If a function has been deprecated in a newer version of Drafting,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef CHARTS_VERSION_MIN_REQUIRED
# define CHARTS_VERSION_MIN_REQUIRED (CHARTS_VERSION_CUR_STABLE)
#endif

/**
 * CHARTS_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the libcharts.h header.

 * The definition should be one of the predefined Drafting version
 * macros: %CHARTS_VERSION_1_0, %CHARTS_VERSION_1_2,...
 *
 * This macro defines the upper bound for the Drafting API to use.
 *
 * If a function has been introduced in a newer version of Drafting,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef CHARTS_VERSION_MAX_ALLOWED
# if CHARTS_VERSION_MIN_REQUIRED > CHARTS_VERSION_PREV_STABLE
#  define CHARTS_VERSION_MAX_ALLOWED (CHARTS_VERSION_MIN_REQUIRED)
# else
#  define CHARTS_VERSION_MAX_ALLOWED (CHARTS_VERSION_CUR_STABLE)
# endif
#endif

#if CHARTS_VERSION_MAX_ALLOWED < CHARTS_VERSION_MIN_REQUIRED
#error "CHARTS_VERSION_MAX_ALLOWED must be >= CHARTS_VERSION_MIN_REQUIRED"
#endif
#if CHARTS_VERSION_MIN_REQUIRED < CHARTS_VERSION_1_0
#error "CHARTS_VERSION_MIN_REQUIRED must be >= CHARTS_VERSION_1_0"
#endif

#define CHARTS_AVAILABLE_IN_ALL                  _CHARTS_EXTERN

#if CHARTS_VERSION_MIN_REQUIRED >= CHARTS_VERSION_1_0
# define CHARTS_DEPRECATED_IN_1_0                CHARTS_DEPRECATED
# define CHARTS_DEPRECATED_IN_1_0_FOR(f)         CHARTS_DEPRECATED_FOR(f)
#else
# define CHARTS_DEPRECATED_IN_1_0                _CHARTS_EXTERN
# define CHARTS_DEPRECATED_IN_1_0_FOR(f)         _CHARTS_EXTERN
#endif
#if CHARTS_VERSION_MAX_ALLOWED < CHARTS_VERSION_1_0
# define CHARTS_AVAILABLE_IN_1_0                 CHARTS_UNAVAILABLE(1, 0)
#else
# define CHARTS_AVAILABLE_IN_1_0                 _CHARTS_EXTERN
#endif

CHARTS_AVAILABLE_IN_ALL
guint    charts_get_major_version (void);
CHARTS_AVAILABLE_IN_ALL
guint    charts_get_minor_version (void);
CHARTS_AVAILABLE_IN_ALL
guint    charts_get_micro_version (void);
CHARTS_AVAILABLE_IN_ALL
gboolean charts_check_version     (guint major,
                                   guint minor,
                                   guint micro);

G_END_DECLS
