/*
 * charts-xy-chart.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-axis.h"
#include "charts-internal.h"
#include "charts-normalized-series.h"
#include "charts-series.h"
#include "charts-xy-chart.h"
#include "charts-xy-series.h"

G_DEFINE_ABSTRACT_TYPE (ChartsXYChart, charts_xy_chart, CHARTS_TYPE_CHART)

enum {
  PROP_0,
  PROP_X_AXIS,
  PROP_Y_AXIS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
charts_xy_chart_add_series (ChartsChart  *chart,
                            ChartsSeries *series)
{
  ChartsXYChart *self = (ChartsXYChart *)chart;
  ChartsNormalizedSeries *x_normalized;
  ChartsNormalizedSeries *y_normalized;

  g_assert (CHARTS_IS_XY_CHART (self));
  g_assert (CHARTS_IS_SERIES (series));

  if (!CHARTS_IS_XY_SERIES (series))
    {
      g_warning ("Attempt to add a %s to %s which is not supported",
                 G_OBJECT_TYPE_NAME (series),
                 G_OBJECT_TYPE_NAME (self));
      return;
    }

  x_normalized = g_object_new (CHARTS_TYPE_NORMALIZED_SERIES,
                               "axis", self->x_axis,
                               "expression", charts_xy_series_get_x_expression (CHARTS_XY_SERIES (series)),
                               "model", series,
                               "series", series,
                               NULL);

  y_normalized = g_object_new (CHARTS_TYPE_NORMALIZED_SERIES,
                               "axis", self->y_axis,
                               "expression", charts_xy_series_get_y_expression (CHARTS_XY_SERIES (series)),
                               "model", series,
                               "series", series,
                               NULL);

  g_ptr_array_add (self->x_normalized, x_normalized);
  g_ptr_array_add (self->y_normalized, y_normalized);

  g_object_bind_property (self, "x-axis", x_normalized, "axis", 0);
  g_object_bind_property (self, "y-axis", y_normalized, "axis", 0);

  g_object_bind_property (series, "x-expression", x_normalized, "expression", 0);
  g_object_bind_property (series, "y-expression", y_normalized, "expression", 0);

  g_signal_connect_object (x_normalized,
                           "changed",
                           G_CALLBACK (gtk_widget_queue_draw),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (y_normalized,
                           "changed",
                           G_CALLBACK (gtk_widget_queue_draw),
                           self,
                           G_CONNECT_SWAPPED);

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
charts_xy_chart_remove_series (ChartsChart  *chart,
                               ChartsSeries *series)
{
  ChartsXYChart *self = (ChartsXYChart *)chart;

  g_assert (CHARTS_IS_XY_CHART (self));
  g_assert (CHARTS_IS_SERIES (series));

  for (guint i = self->x_normalized->len; i > 0; i--)
    {
      ChartsNormalizedSeries *normal = g_ptr_array_index (self->x_normalized, i-1);

      if (charts_normalized_series_get_series (normal) == series)
        g_ptr_array_remove_index (self->x_normalized, i-1);
    }

  for (guint i = self->y_normalized->len; i > 0; i--)
    {
      ChartsNormalizedSeries *normal = g_ptr_array_index (self->y_normalized, i-1);

      if (charts_normalized_series_get_series (normal) == series)
        g_ptr_array_remove_index (self->y_normalized, i-1);
    }

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
charts_xy_chart_snapshot (GtkWidget   *widget,
                          GtkSnapshot *snapshot)
{
  ChartsXYChart *self = (ChartsXYChart *)widget;

  g_assert (CHARTS_IS_CHART (self));
  g_assert (GTK_IS_SNAPSHOT (snapshot));
  g_assert (self->x_normalized->len == self->y_normalized->len);

  for (guint i = 0; i < self->x_normalized->len; i++)
    {
      ChartsNormalizedSeries *x_normalized = g_ptr_array_index (self->x_normalized, i);
      ChartsNormalizedSeries *y_normalized = g_ptr_array_index (self->y_normalized, i);

      CHARTS_XY_CHART_GET_CLASS (self)->snapshot_xy (self, snapshot, x_normalized, y_normalized);
    }
}

static void
charts_xy_chart_finalize (GObject *object)
{
  ChartsXYChart *self = (ChartsXYChart *)object;

  g_clear_pointer (&self->x_normalized, g_ptr_array_unref);
  g_clear_pointer (&self->y_normalized, g_ptr_array_unref);
  g_clear_object (&self->x_axis);
  g_clear_object (&self->y_axis);

  G_OBJECT_CLASS (charts_xy_chart_parent_class)->finalize (object);
}

static void
charts_xy_chart_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  ChartsXYChart *self = CHARTS_XY_CHART (object);

  switch (prop_id)
    {
    case PROP_X_AXIS:
      g_value_set_object (value, charts_xy_chart_get_x_axis (self));
      break;

    case PROP_Y_AXIS:
      g_value_set_object (value, charts_xy_chart_get_y_axis (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_xy_chart_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  ChartsXYChart *self = CHARTS_XY_CHART (object);

  switch (prop_id)
    {
    case PROP_X_AXIS:
      charts_xy_chart_set_x_axis (self, g_value_get_object (value));
      break;

    case PROP_Y_AXIS:
      charts_xy_chart_set_y_axis (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_xy_chart_class_init (ChartsXYChartClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  ChartsChartClass *chart_class = CHARTS_CHART_CLASS (klass);

  object_class->finalize = charts_xy_chart_finalize;
  object_class->get_property = charts_xy_chart_get_property;
  object_class->set_property = charts_xy_chart_set_property;

  widget_class->snapshot = charts_xy_chart_snapshot;

  chart_class->add_series = charts_xy_chart_add_series;
  chart_class->remove_series = charts_xy_chart_remove_series;

  properties [PROP_X_AXIS] =
    g_param_spec_object ("x-axis", NULL, NULL,
                         CHARTS_TYPE_AXIS,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_Y_AXIS] =
    g_param_spec_object ("y-axis", NULL, NULL,
                         CHARTS_TYPE_AXIS,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
charts_xy_chart_init (ChartsXYChart *self)
{
  self->x_normalized = g_ptr_array_new_with_free_func (g_object_unref);
  self->y_normalized = g_ptr_array_new_with_free_func (g_object_unref);
}

/**
 * charts_xy_chart_get_x_axis:
 * @self: a #ChartsChart
 *
 * Gets the X axis.
 *
 * Returns: (transfer none) (nullable): a #ChartsAxis or %NULL
 */
ChartsAxis *
charts_xy_chart_get_x_axis (ChartsXYChart *self)
{
  g_return_val_if_fail (CHARTS_IS_XY_CHART (self), NULL);

  return self->x_axis;
}

/**
 * charts_xy_chart_get_y_axis:
 * @self: a #ChartsChart
 *
 * Gets the Y axis.
 *
 * Returns: (transfer none) (nullable): a #ChartsAxis or %NULL
 */
ChartsAxis *
charts_xy_chart_get_y_axis (ChartsXYChart *self)
{
  g_return_val_if_fail (CHARTS_IS_XY_CHART (self), NULL);

  return self->y_axis;
}

void
charts_xy_chart_set_x_axis (ChartsXYChart *self,
                            ChartsAxis    *x_axis)
{
  g_return_if_fail (CHARTS_IS_XY_CHART (self));
  g_return_if_fail (!x_axis || CHARTS_IS_AXIS (x_axis));

  if (g_set_object (&self->x_axis, x_axis))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_X_AXIS]);
}

void
charts_xy_chart_set_y_axis (ChartsXYChart *self,
                            ChartsAxis    *y_axis)
{
  g_return_if_fail (CHARTS_IS_XY_CHART (self));
  g_return_if_fail (!y_axis || CHARTS_IS_AXIS (y_axis));

  if (g_set_object (&self->y_axis, y_axis))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_Y_AXIS]);
}
