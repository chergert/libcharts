/*
 * charts-line-chart.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-line-chart.h"
#include "charts-line-series.h"
#include "charts-normalized-series.h"

G_DEFINE_TYPE (ChartsLineChart, charts_line_chart, CHARTS_TYPE_XY_CHART)

/**
 * charts_line_chart_new:
 *
 * Create a new #ChartsLineChart.
 *
 * Returns: (transfer full): a newly created #ChartsLineChart
 */
GtkWidget *
charts_line_chart_new (void)
{
  return g_object_new (CHARTS_TYPE_LINE_CHART, NULL);
}

static void
charts_line_chart_snapshot_xy (ChartsXYChart          *xy_chart,
                               GtkSnapshot            *snapshot,
                               ChartsNormalizedSeries *x_normalized,
                               ChartsNormalizedSeries *y_normalized)
{
  ChartsLineChart *self = (ChartsLineChart *)xy_chart;
  const GdkRGBA *color;
  const GdkRGBA *fill_color;
  ChartsSeries *series;
  graphene_rect_t area;
  const float *x_values;
  const float *y_values;
  ChartsAxis *y_axis;
  double line_width;
  cairo_t *cr;
  GValue min_value = G_VALUE_INIT;
  guint n_x_values;
  guint n_y_values;
  guint n_values;
  float y0;

  g_assert (CHARTS_IS_LINE_CHART (self));
  g_assert (GTK_IS_SNAPSHOT (snapshot));
  g_assert (CHARTS_IS_NORMALIZED_SERIES (x_normalized));
  g_assert (CHARTS_IS_NORMALIZED_SERIES (y_normalized));

  x_values = charts_normalized_series_get_values (x_normalized, &n_x_values);
  y_values = charts_normalized_series_get_values (y_normalized, &n_y_values);
  n_values = MIN (n_x_values, n_y_values);

  if (n_values == 0)
    return;

  area.origin.x = 0;
  area.origin.y = 0;
  area.size.width = gtk_widget_get_width (GTK_WIDGET (self));
  area.size.height = gtk_widget_get_height (GTK_WIDGET (self));

  if (area.size.width == 0 || area.size.height == 0)
    return;

  series = charts_normalized_series_get_series (x_normalized);
  color = charts_xy_series_get_color (CHARTS_XY_SERIES (series));
  fill_color = charts_line_series_get_fill_color (CHARTS_LINE_SERIES (series));
  line_width = charts_line_series_get_line_width (CHARTS_LINE_SERIES (series));

  /* Your y0 may be above you. Figure that out. */
  y_axis = charts_xy_chart_get_y_axis (CHARTS_XY_CHART (self));
  _charts_axis_get_min_value (y_axis, &min_value);
  y0 = _charts_axis_normalize (y_axis, &min_value) * area.size.height;
  g_value_unset (&min_value);

  cr = gtk_snapshot_append_cairo (snapshot, &area);

  cairo_set_matrix (cr, &(cairo_matrix_t) {1, 0, 0, -1, 0, area.size.height});

  if (fill_color->alpha > 0.)
    {
      float last_x = x_values[0] * area.size.width;
      float last_y = y_values[0] * area.size.height;

      cairo_move_to (cr, last_x, y0);
      cairo_line_to (cr, last_x, last_y);

      for (guint i = 1; i < n_values; i++)
        {
          float x = x_values[i] * area.size.width;
          float y = y_values[i] * area.size.height;

          cairo_line_to (cr, x, y);

          last_x = x;
          last_y = y;
        }

      cairo_line_to (cr, last_x, y0);

      gdk_cairo_set_source_rgba (cr, fill_color);
      cairo_fill (cr);
    }

  cairo_move_to (cr,
                 x_values[0] * area.size.width,
                 y_values[0] * area.size.height);

  for (guint i = 1; i < n_values; i++)
    {
      float x = x_values[i] * area.size.width;
      float y = y_values[i] * area.size.height;

      cairo_line_to (cr, x, y);
    }

  gdk_cairo_set_source_rgba (cr, color);
  cairo_set_line_width (cr, line_width);
  cairo_stroke (cr);

  cairo_destroy (cr);
}

static void
charts_line_chart_class_init (ChartsLineChartClass *klass)
{
  ChartsXYChartClass *xy_chart_class = CHARTS_XY_CHART_CLASS (klass);

  xy_chart_class->snapshot_xy = charts_line_chart_snapshot_xy;
}

static void
charts_line_chart_init (ChartsLineChart *self)
{
}
