/* charts-xy-series.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-xy-series.h"

enum {
  PROP_0,
  PROP_COLOR,
  PROP_X_EXPRESSION,
  PROP_Y_EXPRESSION,
  N_PROPS
};

G_DEFINE_TYPE (ChartsXYSeries, charts_xy_series, CHARTS_TYPE_SERIES)

static GParamSpec *properties[N_PROPS];
static const GdkRGBA default_color = {0,0,0,1};

static void
charts_xy_series_finalize (GObject *object)
{
  ChartsXYSeries *self = (ChartsXYSeries *)object;

  g_clear_pointer (&self->x_expression, gtk_expression_unref);
  g_clear_pointer (&self->y_expression, gtk_expression_unref);

  G_OBJECT_CLASS (charts_xy_series_parent_class)->finalize (object);
}

static void
charts_xy_series_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ChartsXYSeries *self = CHARTS_XY_SERIES (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      g_value_set_boxed (value, charts_xy_series_get_color (self));
      break;

    case PROP_X_EXPRESSION:
      gtk_value_set_expression (value, charts_xy_series_get_x_expression (self));
      break;

    case PROP_Y_EXPRESSION:
      gtk_value_set_expression (value, charts_xy_series_get_y_expression (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_xy_series_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ChartsXYSeries *self = CHARTS_XY_SERIES (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      charts_xy_series_set_color (self, g_value_get_boxed (value));
      break;

    case PROP_X_EXPRESSION:
      charts_xy_series_set_x_expression (self, gtk_value_get_expression (value));
      break;

    case PROP_Y_EXPRESSION:
      charts_xy_series_set_y_expression (self, gtk_value_get_expression (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_xy_series_class_init (ChartsXYSeriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = charts_xy_series_finalize;
  object_class->get_property = charts_xy_series_get_property;
  object_class->set_property = charts_xy_series_set_property;

  properties[PROP_COLOR] =
    g_param_spec_boxed ("color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_X_EXPRESSION] =
    gtk_param_spec_expression ("x-expression", NULL, NULL,
                               (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_Y_EXPRESSION] =
    gtk_param_spec_expression ("y-expression", NULL, NULL,
                               (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
charts_xy_series_init (ChartsXYSeries *self)
{
}

ChartsXYSeries *
charts_xy_series_new (void)
{
  return g_object_new (CHARTS_TYPE_XY_SERIES, NULL);
}

const GdkRGBA *
charts_xy_series_get_color (ChartsXYSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_XY_SERIES (self), NULL);

  return &self->color;
}

void
charts_xy_series_set_color (ChartsXYSeries *self,
                            const GdkRGBA  *color)
{
  g_return_if_fail (CHARTS_IS_XY_SERIES (self));

  if (color == NULL)
    color = &default_color;

  if (!gdk_rgba_equal (&self->color, color))
    {
      self->color = *color;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_COLOR]);
    }
}

/**
 * charts_xy_series_get_x_expression:
 * @self: a #ChartsXYSeries
 *
 * Returns: (transfer none) (nullable): a #GtkExpression or %NULL
 */
GtkExpression *
charts_xy_series_get_x_expression (ChartsXYSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_XY_SERIES (self), NULL);

  return self->x_expression;
}

/**
 * charts_xy_series_get_y_expression:
 * @self: a #ChartsXYSeries
 *
 * Returns: (transfer none) (nullable): a #GtkExpression or %NULL
 */
GtkExpression *
charts_xy_series_get_y_expression (ChartsXYSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_XY_SERIES (self), NULL);

  return self->y_expression;
}

void
charts_xy_series_set_x_expression (ChartsXYSeries *self,
                                   GtkExpression  *x_expression)
{
  GtkExpression *old = NULL;

  g_return_if_fail (CHARTS_IS_XY_SERIES (self));

  if (x_expression == self->x_expression)
    return;

  old = g_steal_pointer (&self->x_expression);

  if (x_expression)
    self->x_expression = gtk_expression_ref (x_expression);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_X_EXPRESSION]);

  g_clear_pointer (&old, gtk_expression_unref);
}

void
charts_xy_series_set_y_expression (ChartsXYSeries *self,
                                   GtkExpression  *y_expression)
{
  GtkExpression *old = NULL;

  g_return_if_fail (CHARTS_IS_XY_SERIES (self));

  if (y_expression == self->y_expression)
    return;

  old = g_steal_pointer (&self->y_expression);

  if (y_expression)
    self->y_expression = gtk_expression_ref (y_expression);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_Y_EXPRESSION]);

  g_clear_pointer (&old, gtk_expression_unref);
}
