/* charts-version.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-version.h"
#include "charts-version-macros.h"

guint
charts_get_major_version (void)
{
  return CHARTS_MAJOR_VERSION;
}

guint
charts_get_micro_version (void)
{
  return CHARTS_MICRO_VERSION;
}

guint
charts_get_minor_version (void)
{
  return CHARTS_MINOR_VERSION;
}

gboolean
charts_check_version (guint major,
                      guint minor,
                      guint micro)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtype-limits"
  return CHARTS_CHECK_VERSION (major, minor, micro);
#pragma GCC diagnostic pop
}
