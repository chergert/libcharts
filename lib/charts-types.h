/*
 * charts-types.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _ChartsAxis                  ChartsAxis;
typedef struct _ChartsAxisClass             ChartsAxisClass;
typedef struct _ChartsChart                 ChartsChart;
typedef struct _ChartsChartClass            ChartsChartClass;
typedef struct _ChartsDateTimeAxis          ChartsDateTimeAxis;
typedef struct _ChartsDateTimeAxisClass     ChartsDateTimeAxisClass;
typedef struct _ChartsLineChart             ChartsLineChart;
typedef struct _ChartsLineChartClass        ChartsLineChartClass;
typedef struct _ChartsLineSeries            ChartsLineSeries;
typedef struct _ChartsLineSeriesClass       ChartsLineSeriesClass;
typedef struct _ChartsNormalizedSeries      ChartsNormalizedSeries;
typedef struct _ChartsNormalizedSeriesClass ChartsNormalizedSeriesClass;
typedef struct _ChartsSeries                ChartsSeries;
typedef struct _ChartsSeriesClass           ChartsSeriesClass;
typedef struct _ChartsSplineChart           ChartsSplineChart;
typedef struct _ChartsSplineChartClass      ChartsSplineChartClass;
typedef struct _ChartsSplineSeries          ChartsSplineSeries;
typedef struct _ChartsSplineSeriesClass     ChartsSplineSeriesClass;
typedef struct _ChartsValueAxis             ChartsValueAxis;
typedef struct _ChartsValueAxisClass        ChartsValueAxisClass;
typedef struct _ChartsXYChart               ChartsXYChart;
typedef struct _ChartsXYChartClass          ChartsXYChartClass;
typedef struct _ChartsXYSeries              ChartsXYSeries;
typedef struct _ChartsXYSeriesClass         ChartsXYSeriesClass;

G_END_DECLS
