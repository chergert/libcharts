/*
 * charts-normalized-series.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-axis.h"
#include "charts-internal.h"
#include "charts-normalized-series.h"
#include "charts-series.h"

#define CHARTS_NORMALIZED_SERIES_STEP_TIME_USEC (1000) /* 1 msec */

G_DEFINE_FINAL_TYPE (ChartsNormalizedSeries, charts_normalized_series, CHARTS_TYPE_SERIES)

enum {
  PROP_0,
  PROP_AXIS,
  PROP_EXPRESSION,
  PROP_SERIES,
  N_PROPS
};

enum {
  CHANGED,
  N_SIGNALS
};

static GParamSpec *properties[N_PROPS];
static guint signals[N_SIGNALS];

ChartsNormalizedSeries *
charts_normalized_series_new (ChartsSeries  *series,
                              GtkExpression *expression,
                              ChartsAxis    *axis)
{
  g_return_val_if_fail (CHARTS_IS_SERIES (series), NULL);
  g_return_val_if_fail (CHARTS_IS_AXIS (axis), NULL);
  g_return_val_if_fail (GTK_IS_EXPRESSION (expression), NULL);

  return g_object_new (CHARTS_TYPE_NORMALIZED_SERIES,
                       "model", series,
                       "axis", axis,
                       "expression", expression,
                       NULL);
}

static gboolean
charts_normalized_series_update_missing (gpointer user_data)
{
  ChartsNormalizedSeries *self = user_data;
  GListModel *model;
  GtkBitsetIter iter;
  guint position;

  g_assert (CHARTS_IS_NORMALIZED_SERIES (self));

  model = G_LIST_MODEL (self);

  if (gtk_bitset_iter_init_first (&iter, self->missing, &position))
    {
      gint64 deadline = g_get_monotonic_time () + CHARTS_NORMALIZED_SERIES_STEP_TIME_USEC;

      do
        {
          GObject *item = g_list_model_get_item (model, position);
          GValue value = G_VALUE_INIT;

          gtk_expression_evaluate (self->expression, item, &value);

          g_array_index (self->values, float, position) = _charts_axis_normalize (self->axis, &value);

          gtk_bitset_remove (self->missing, position);

          g_value_unset (&value);
          g_clear_object (&item);
        }
      while (gtk_bitset_iter_next (&iter, &position) &&
             g_get_monotonic_time () < deadline);
    }

  g_signal_emit (self, signals[CHANGED], 0);

  if (gtk_bitset_is_empty (self->missing))
    {
      self->update_source = 0;
      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

static void
charts_normalized_series_maybe_update (ChartsNormalizedSeries *self)
{
  GSource *source;

  g_assert (CHARTS_IS_NORMALIZED_SERIES (self));

  if (self->update_source)
    return;

  if (gtk_bitset_is_empty (self->missing))
    return;

  source = g_idle_source_new ();
  g_source_set_callback (source, charts_normalized_series_update_missing, self, NULL);
  g_source_set_static_name (source, "[ChartsNormalizedSeries]");
  g_source_set_priority (source, G_PRIORITY_LOW);
  self->update_source = g_source_attach (source, NULL);
  g_source_unref (source);
}

static void
charts_normalized_series_items_changed (ChartsSeries *series,
                                        GListModel   *model,
                                        guint         position,
                                        guint         removed,
                                        guint         added)
{
  ChartsNormalizedSeries *self = (ChartsNormalizedSeries *)series;

  g_assert (CHARTS_IS_NORMALIZED_SERIES (self));
  g_assert (G_IS_LIST_MODEL (model));

  gtk_bitset_splice (self->missing, position, removed, added);
  gtk_bitset_add_range (self->missing, position, added);

  if (removed > 0)
    g_array_remove_range (self->values, position, removed);

  if (added > 0)
    {
      if (position == self->values->len)
        {
          g_array_set_size (self->values, self->values->len + added);
        }
      else
        {
          static const float empty[32] = {0};
          const float *vals = empty;
          float *alloc = NULL;

          if (added > G_N_ELEMENTS (empty))
            vals = alloc = g_new0 (float, added);

          g_array_insert_vals (self->values, position, vals, added);

          g_free (alloc);
        }
    }

  CHARTS_SERIES_CLASS (charts_normalized_series_parent_class)->items_changed (series, model, position, removed, added);

  charts_normalized_series_maybe_update (self);
}

static void
charts_normalized_series_invalidate (ChartsNormalizedSeries *self)
{
  guint n_items;

  g_assert (CHARTS_IS_NORMALIZED_SERIES (self));

  n_items = g_list_model_get_n_items (G_LIST_MODEL (self));

  if (n_items > 0)
    CHARTS_SERIES_GET_CLASS (self)->items_changed (CHARTS_SERIES (self),
                                                   G_LIST_MODEL (self->series),
                                                   0,
                                                   n_items,
                                                   n_items);
}

static void
charts_normalized_series_constructed (GObject *object)
{
  ChartsNormalizedSeries *self = (ChartsNormalizedSeries *)object;

  G_OBJECT_CLASS (charts_normalized_series_parent_class)->constructed (object);

  charts_normalized_series_invalidate (self);
}

static void
charts_normalized_series_dispose (GObject *object)
{
  ChartsNormalizedSeries *self = (ChartsNormalizedSeries *)object;

  g_clear_handle_id (&self->update_source, g_source_remove);

  g_clear_signal_handler (&self->range_changed_handler, self->axis);
  g_clear_object (&self->axis);

  g_clear_object (&self->series);

  g_clear_pointer (&self->expression, gtk_expression_unref);
  g_clear_pointer (&self->missing, gtk_bitset_unref);
  g_clear_pointer (&self->values, g_array_unref);

  G_OBJECT_CLASS (charts_normalized_series_parent_class)->dispose (object);
}

static void
charts_normalized_series_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  ChartsNormalizedSeries *self = CHARTS_NORMALIZED_SERIES (object);

  switch (prop_id)
    {
    case PROP_AXIS:
      g_value_set_object (value, charts_normalized_series_get_axis (self));
      break;

    case PROP_EXPRESSION:
      gtk_value_set_expression (value, charts_normalized_series_get_expression (self));
      break;

    case PROP_SERIES:
      g_value_set_object (value, charts_normalized_series_get_series (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_normalized_series_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  ChartsNormalizedSeries *self = CHARTS_NORMALIZED_SERIES (object);

  switch (prop_id)
    {
    case PROP_AXIS:
      charts_normalized_series_set_axis (self, g_value_get_object (value));
      break;

    case PROP_EXPRESSION:
      charts_normalized_series_set_expression (self, gtk_value_dup_expression (value));
      break;

    case PROP_SERIES:
      charts_normalized_series_set_series (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_normalized_series_class_init (ChartsNormalizedSeriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ChartsSeriesClass *series_class = CHARTS_SERIES_CLASS (klass);

  object_class->constructed = charts_normalized_series_constructed;
  object_class->dispose = charts_normalized_series_dispose;
  object_class->get_property = charts_normalized_series_get_property;
  object_class->set_property = charts_normalized_series_set_property;

  series_class->items_changed = charts_normalized_series_items_changed;

  properties[PROP_AXIS] =
    g_param_spec_object ("axis", NULL, NULL,
                         CHARTS_TYPE_AXIS,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_EXPRESSION] =
    gtk_param_spec_expression ("expression", NULL, NULL,
                               (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_SERIES] =
    g_param_spec_object ("series", NULL, NULL,
                         CHARTS_TYPE_SERIES,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals[CHANGED] =
    g_signal_new ("changed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
charts_normalized_series_init (ChartsNormalizedSeries *self)
{
  self->missing = gtk_bitset_new_empty ();
  self->values = g_array_new (FALSE, TRUE, sizeof (float));
}

const float *
charts_normalized_series_get_values (ChartsNormalizedSeries *self,
                                     guint                  *n_values)
{
  g_assert (CHARTS_IS_NORMALIZED_SERIES (self));
  g_assert (n_values != NULL);

  if (self->values->len == 0)
    return NULL;

  *n_values = self->values->len;

  return (const float *)(gpointer)self->values->data;
}

/**
 * charts_normalized_series_get_series:
 * @self: a #ChartsNormalizedSeries
 *
 * Returns: (transfer none) (nullable): a #ChartsSeries
 */
ChartsSeries *
charts_normalized_series_get_series (ChartsNormalizedSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_NORMALIZED_SERIES (self), NULL);

  return self->series;
}

void
charts_normalized_series_set_series (ChartsNormalizedSeries *self,
                                     ChartsSeries           *series)
{
  g_return_if_fail (CHARTS_IS_NORMALIZED_SERIES (self));
  g_return_if_fail (!series || CHARTS_IS_SERIES (series));

  if (g_set_object (&self->series, series))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SERIES]);
}

/**
 * charts_normalized_series_get_axis:
 * @self: a #ChartsNormalizedSeries
 *
 * Returns: (transfer none) (nullable): a #ChartsAxis
 */
ChartsAxis *
charts_normalized_series_get_axis (ChartsNormalizedSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_NORMALIZED_SERIES (self), NULL);

  return self->axis;
}

void
charts_normalized_series_set_axis (ChartsNormalizedSeries *self,
                                   ChartsAxis             *axis)
{
  g_return_if_fail (CHARTS_IS_NORMALIZED_SERIES (self));
  g_return_if_fail (!axis || CHARTS_IS_AXIS (axis));

  if (self->axis == axis)
    return;

  if (axis)
    g_object_ref (axis);

  if (self->axis)
    {
      g_clear_signal_handler (&self->range_changed_handler, self->axis);
      g_clear_object (&self->axis);
    }

  if (axis)
    {
      self->axis = g_object_ref (axis);
      self->range_changed_handler =
        g_signal_connect_object (axis,
                                 "range-changed",
                                 G_CALLBACK (charts_normalized_series_invalidate),
                                 self,
                                 G_CONNECT_SWAPPED);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_AXIS]);

  charts_normalized_series_invalidate (self);
}

/**
 * charts_normalized_series_get_expression:
 * @self: a #ChartsNormalizedSeries
 *
 * Get the expression to extract values from the model.
 *
 * Returns: (transfer none) (nullable): a #GtkExpression or %NULL
 */
GtkExpression *
charts_normalized_series_get_expression (ChartsNormalizedSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_NORMALIZED_SERIES (self), NULL);

  return self->expression;
}

void
charts_normalized_series_set_expression (ChartsNormalizedSeries *self,
                                         GtkExpression          *expression)
{
  g_return_if_fail (CHARTS_IS_NORMALIZED_SERIES (self));

  if (self->expression == expression)
    return;

  if (expression)
    gtk_expression_ref (expression);

  g_clear_pointer (&self->expression, gtk_expression_unref);
  self->expression = expression;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_EXPRESSION]);

  charts_normalized_series_invalidate (self);
}
