/* charts-xy-series.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "charts-series.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_XY_SERIES         (charts_xy_series_get_type())
#define CHARTS_IS_XY_SERIES(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_XY_SERIES))
#define CHARTS_XY_SERIES(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_XY_SERIES, ChartsXYSeries))
#define CHARTS_XY_SERIES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_XY_SERIES, ChartsXYSeriesClass))

typedef struct _ChartsXYSeries ChartsXYSeries;
typedef struct _ChartsXYSeriesClass ChartsXYSeriesClass;

CHARTS_AVAILABLE_IN_ALL
GType           charts_xy_series_get_type         (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsXYSeries *charts_xy_series_new              (void);
CHARTS_AVAILABLE_IN_ALL
GtkExpression  *charts_xy_series_get_x_expression (ChartsXYSeries *self);
CHARTS_AVAILABLE_IN_ALL
void            charts_xy_series_set_x_expression (ChartsXYSeries *self,
                                                   GtkExpression  *x_expression);
CHARTS_AVAILABLE_IN_ALL
GtkExpression  *charts_xy_series_get_y_expression (ChartsXYSeries *self);
CHARTS_AVAILABLE_IN_ALL
void            charts_xy_series_set_y_expression (ChartsXYSeries *self,
                                                   GtkExpression  *y_expression);
CHARTS_AVAILABLE_IN_ALL
const GdkRGBA  *charts_xy_series_get_color        (ChartsXYSeries *self);
CHARTS_AVAILABLE_IN_ALL
void            charts_xy_series_set_color        (ChartsXYSeries *self,
                                                   const GdkRGBA  *color);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsXYSeries, g_object_unref)

G_END_DECLS
