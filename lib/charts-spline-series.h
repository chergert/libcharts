/* charts-spline-series.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-types.h"
#include "charts-xy-series.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_SPLINE_SERIES         (charts_spline_series_get_type())
#define CHARTS_IS_SPLINE_SERIES(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_SPLINE_SERIES))
#define CHARTS_SPLINE_SERIES(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_SPLINE_SERIES, ChartsSplineSeries))
#define CHARTS_SPLINE_SERIES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_SPLINE_SERIES, ChartsSplineSeriesClass))

typedef struct _ChartsSplineSeries ChartsSplineSeries;
typedef struct _ChartsSplineSeriesClass ChartsSplineSeriesClass;

CHARTS_AVAILABLE_IN_ALL
GType               charts_spline_series_get_type (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsSplineSeries *charts_spline_series_new      (void);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsSplineSeries, g_object_unref)

G_END_DECLS

