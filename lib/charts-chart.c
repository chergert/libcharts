/* charts-chart.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-chart.h"
#include "charts-internal.h"
#include "charts-series.h"

enum {
  PROP_0,
  PROP_TITLE,
  N_PROPS
};

static GtkBuildableIface *parent_buildable;

static void
charts_chart_add_child (GtkBuildable *buildable,
                        GtkBuilder   *builder,
                        GObject      *child,
                        const char   *type)
{
  if (CHARTS_IS_SERIES (child))
    charts_chart_add_series (CHARTS_CHART (buildable), CHARTS_SERIES (child));
  else
    parent_buildable->add_child (buildable, builder, child, type);
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  parent_buildable = g_type_interface_peek_parent (iface);
  iface->add_child = charts_chart_add_child;
}

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (ChartsChart, charts_chart, GTK_TYPE_WIDGET,
                                  G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

static GParamSpec *properties [N_PROPS];

static void
charts_chart_finalize (GObject *object)
{
  ChartsChart *self = (ChartsChart *)object;

  g_clear_pointer (&self->series, g_ptr_array_unref);
  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (charts_chart_parent_class)->finalize (object);
}

static void
charts_chart_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ChartsChart *self = CHARTS_CHART (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, charts_chart_get_title (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_chart_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ChartsChart *self = CHARTS_CHART (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      charts_chart_set_title (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_chart_class_init (ChartsChartClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = charts_chart_finalize;
  object_class->get_property = charts_chart_get_property;
  object_class->set_property = charts_chart_set_property;

  properties[PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
charts_chart_init (ChartsChart *self)
{
  self->series = g_ptr_array_new_with_free_func (g_object_unref);
}

const char *
charts_chart_get_title (ChartsChart *self)
{
  g_return_val_if_fail (CHARTS_IS_CHART (self), NULL);

  return self->title;
}

void
charts_chart_set_title (ChartsChart *self,
                        const char *title)
{
  g_return_if_fail (CHARTS_IS_CHART (self));

  if (g_set_str (&self->title, title))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TITLE]);
}

void
charts_chart_add_series (ChartsChart  *self,
                         ChartsSeries *series)
{
  g_return_if_fail (CHARTS_IS_CHART (self));
  g_return_if_fail (CHARTS_IS_SERIES (series));

  g_ptr_array_add (self->series, g_object_ref (series));

  if (CHARTS_CHART_GET_CLASS (self)->add_series)
    CHARTS_CHART_GET_CLASS (self)->add_series (self, series);
}

void
charts_chart_remove_series (ChartsChart  *self,
                            ChartsSeries *series)
{
  g_return_if_fail (CHARTS_IS_CHART (self));
  g_return_if_fail (CHARTS_IS_SERIES (series));

  g_object_ref (series);

  g_ptr_array_remove (self->series, series);

  if (CHARTS_CHART_GET_CLASS (self)->remove_series)
    CHARTS_CHART_GET_CLASS (self)->remove_series (self, series);

  g_object_unref (series);
}
