/* charts-date-time-axis.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-types.h"
#include "charts-axis.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_DATE_TIME_AXIS         (charts_date_time_axis_get_type())
#define CHARTS_IS_DATE_TIME_AXIS(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_DATE_TIME_AXIS))
#define CHARTS_DATE_TIME_AXIS(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_DATE_TIME_AXIS, ChartsDateTimeAxis))
#define CHARTS_DATE_TIME_AXIS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_DATE_TIME_AXIS, ChartsDateTimeAxisClass))

typedef struct _ChartsDateTimeAxis ChartsDateTimeAxis;
typedef struct _ChartsDateTimeAxisClass ChartsDateTimeAxisClass;

CHARTS_AVAILABLE_IN_ALL
GType               charts_date_time_axis_get_type      (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
ChartsDateTimeAxis *charts_date_time_axis_new           (void);
CHARTS_AVAILABLE_IN_ALL
GDateTime          *charts_date_time_axis_get_max_value (ChartsDateTimeAxis *self);
CHARTS_AVAILABLE_IN_ALL
void                charts_date_time_axis_set_max_value (ChartsDateTimeAxis *self,
                                                         GDateTime          *max_value);
CHARTS_AVAILABLE_IN_ALL
GDateTime          *charts_date_time_axis_get_min_value (ChartsDateTimeAxis *self);
CHARTS_AVAILABLE_IN_ALL
void                charts_date_time_axis_set_min_value (ChartsDateTimeAxis *self,
                                                         GDateTime          *min_value);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsDateTimeAxis, g_object_unref)

G_END_DECLS
