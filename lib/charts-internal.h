/*
 * charts-internal.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "charts-types.h"

G_BEGIN_DECLS

struct _ChartsSeries
{
  GObject     parent_instance;
  char       *title;
  GListModel *model;
  gulong      items_changed_handler;
};

struct _ChartsSeriesClass
{
  GObjectClass parent_class;

  void (*items_changed) (ChartsSeries *series,
                         GListModel   *model,
                         guint         position,
                         guint         removed,
                         guint         added);
};

struct _ChartsXYSeries
{
  ChartsSeries parent_instance;
  GtkExpression *x_expression;
  GtkExpression *y_expression;
  GdkRGBA color;
};

struct _ChartsXYSeriesClass
{
  ChartsSeriesClass parent_class;
};

struct _ChartsLineSeries
{
  ChartsXYSeries parent_instance;
  double line_width;
  GdkRGBA fill_color;
};

struct _ChartsLineSeriesClass
{
  ChartsXYSeriesClass parent_class;
};

struct _ChartsSplineSeries
{
  ChartsXYSeries parent_instance;
};

struct _ChartsSplineSeriesClass
{
  ChartsXYSeriesClass parent_class;
};

struct _ChartsNormalizedSeries
{
  ChartsSeries   parent_instance;

  ChartsSeries  *series;
  ChartsAxis    *axis;
  GtkExpression *expression;
  GArray        *values;
  GtkBitset     *missing;

  gulong         range_changed_handler;
  guint          update_source;
};

struct _ChartsNormalizedSeriesClass
{
  ChartsSeriesClass parent_class;
};

struct _ChartsAxis
{
  GObject parent_instance;
  char *title;
};

struct _ChartsAxisClass
{
  GObjectClass parent_class;

  void   (*get_min_value) (ChartsAxis   *axis,
                           GValue       *min_value);
  double (*normalize)     (ChartsAxis   *axis,
                           const GValue *value);
};

struct _ChartsValueAxis
{
  ChartsAxis parent_instance;
  double min_value;
  double max_value;
  double distance;
};

struct _ChartsValueAxisClass
{
  ChartsAxisClass parent_class;
};

struct _ChartsDateTimeAxis
{
  ChartsAxis parent_instance;
  GDateTime *min_value;
  GDateTime *max_value;
  gint64 min_value_unix;
  gint64 max_value_unix;
  gint64 distance_unix;
};

struct _ChartsDateTimeAxisClass
{
  ChartsAxisClass parent_class;
};

struct _ChartsChart
{
  GtkWidget parent_instance;
  char *title;
  GPtrArray *series;
};

struct _ChartsChartClass
{
  GtkWidgetClass parent_class;

  void (*add_series)    (ChartsChart  *chart,
                         ChartsSeries *series);
  void (*remove_series) (ChartsChart  *chart,
                         ChartsSeries *series);
};

struct _ChartsXYChart
{
  ChartsChart parent_instance;
  ChartsAxis *x_axis;
  ChartsAxis *y_axis;
  GPtrArray *x_normalized;
  GPtrArray *y_normalized;
};

struct _ChartsXYChartClass
{
  ChartsChartClass parent_class;

  void (*snapshot_xy) (ChartsXYChart          *self,
                       GtkSnapshot            *snapshot,
                       ChartsNormalizedSeries *x_series,
                       ChartsNormalizedSeries *y_series);
};

struct _ChartsLineChart
{
  ChartsXYChart parent_instance;
};

struct _ChartsLineChartClass
{
  ChartsXYChartClass parent_class;
};

struct _ChartsSplineChart
{
  ChartsXYChart parent_instance;
};

struct _ChartsSplineChartClass
{
  ChartsXYChartClass parent_class;
};

#define CHARTS_AXIS_GET_CLASS(obj)      G_TYPE_INSTANCE_GET_CLASS(obj, CHARTS_TYPE_AXIS, ChartsAxisClass)
#define CHARTS_CHART_GET_CLASS(obj)     G_TYPE_INSTANCE_GET_CLASS(obj, CHARTS_TYPE_CHART, ChartsChartClass)
#define CHARTS_XY_CHART_GET_CLASS(obj)  G_TYPE_INSTANCE_GET_CLASS(obj, CHARTS_TYPE_XY_CHART, ChartsXYChartClass)
#define CHARTS_SERIES_GET_CLASS(obj)    G_TYPE_INSTANCE_GET_CLASS(obj, CHARTS_TYPE_SERIES, ChartsSeriesClass)

static inline void
_charts_axis_get_min_value (ChartsAxis *axis,
                            GValue     *min_value)
{
  CHARTS_AXIS_GET_CLASS (axis)->get_min_value (axis, min_value);
}

static inline double
_charts_axis_normalize (ChartsAxis   *axis,
                        const GValue *value)
{
  return CHARTS_AXIS_GET_CLASS (axis)->normalize (axis, value);
}

void _charts_axis_emit_range_changed (ChartsAxis *self);

G_END_DECLS
