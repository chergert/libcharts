/* charts-line-series.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "charts-internal.h"
#include "charts-line-series.h"

G_DEFINE_TYPE (ChartsLineSeries, charts_line_series, CHARTS_TYPE_XY_SERIES)

enum {
  PROP_0,
  PROP_FILL_COLOR,
  PROP_LINE_WIDTH,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static void
charts_line_series_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  ChartsLineSeries *self = CHARTS_LINE_SERIES (object);

  switch (prop_id)
    {
    case PROP_FILL_COLOR:
      g_value_set_boxed (value, charts_line_series_get_fill_color (self));
      break;

    case PROP_LINE_WIDTH:
      g_value_set_double (value, charts_line_series_get_line_width (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_line_series_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  ChartsLineSeries *self = CHARTS_LINE_SERIES (object);

  switch (prop_id)
    {
    case PROP_FILL_COLOR:
      charts_line_series_set_fill_color (self, g_value_get_boxed (value));
      break;

    case PROP_LINE_WIDTH:
      charts_line_series_set_line_width (self, g_value_get_double (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
charts_line_series_class_init (ChartsLineSeriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = charts_line_series_get_property;
  object_class->set_property = charts_line_series_set_property;

  properties[PROP_FILL_COLOR] =
    g_param_spec_boxed ("fill-color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties[PROP_LINE_WIDTH] =
    g_param_spec_double ("line-width", NULL, NULL,
                         0, G_MAXDOUBLE, 1.0,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
charts_line_series_init (ChartsLineSeries *self)
{
  self->line_width = 1.0;
}

ChartsLineSeries *
charts_line_series_new (void)
{
  return g_object_new (CHARTS_TYPE_LINE_SERIES, NULL);
}

double
charts_line_series_get_line_width (ChartsLineSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_LINE_SERIES (self), .0);

  return self->line_width;
}

void
charts_line_series_set_line_width (ChartsLineSeries *self,
                                   double            line_width)
{
  g_return_if_fail (CHARTS_IS_LINE_SERIES (self));
  g_return_if_fail (line_width >= .0);

  if (line_width != self->line_width)
    {
      self->line_width = line_width;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LINE_WIDTH]);
    }
}

const GdkRGBA *
charts_line_series_get_fill_color (ChartsLineSeries *self)
{
  g_return_val_if_fail (CHARTS_IS_LINE_SERIES (self), NULL);

  return &self->fill_color;
}

void
charts_line_series_set_fill_color (ChartsLineSeries *self,
                                   const GdkRGBA    *fill_color)
{
  g_return_if_fail (CHARTS_IS_LINE_SERIES (self));

  if (fill_color == NULL && self->fill_color.alpha == 0)
    return;

  if (fill_color == NULL || !gdk_rgba_equal (&self->fill_color, fill_color))
    {
      if (fill_color != NULL)
        self->fill_color = *fill_color;
      else
        self->fill_color.alpha = 0;

      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_FILL_COLOR]);
    }
}
