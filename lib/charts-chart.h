/* charts-chart.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (LIBCHARTS_INSIDE) && !defined (LIBCHARTS_COMPILATION)
# error "Only <libcharts.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "charts-types.h"
#include "charts-version-macros.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_CHART         (charts_chart_get_type())
#define CHARTS_IS_CHART(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_CHART))
#define CHARTS_CHART(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_CHART, ChartsChart))
#define CHARTS_CHART_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_CHART, ChartsChartClass))

typedef struct _ChartsChart ChartsChart;
typedef struct _ChartsChartClass ChartsChartClass;

CHARTS_AVAILABLE_IN_ALL
GType       charts_chart_get_type      (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
const char *charts_chart_get_title     (ChartsChart  *self);
CHARTS_AVAILABLE_IN_ALL
void        charts_chart_set_title     (ChartsChart  *self,
                                        const char   *title);
CHARTS_AVAILABLE_IN_ALL
void        charts_chart_add_series    (ChartsChart  *self,
                                        ChartsSeries *series);
CHARTS_AVAILABLE_IN_ALL
void        charts_chart_remove_series (ChartsChart  *self,
                                        ChartsSeries *series);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsChart, g_object_unref)

G_END_DECLS
