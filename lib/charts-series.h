/* charts-series.h
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "charts-types.h"
#include "charts-version-macros.h"

G_BEGIN_DECLS

#define CHARTS_TYPE_SERIES         (charts_series_get_type())
#define CHARTS_IS_SERIES(obj)      (G_TYPE_CHECK_INSTANCE_TYPE(obj, CHARTS_TYPE_SERIES))
#define CHARTS_SERIES(obj)         (G_TYPE_CHECK_INSTANCE_CAST(obj, CHARTS_TYPE_SERIES, ChartsSeries))
#define CHARTS_SERIES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, CHARTS_TYPE_SERIES, ChartsSeriesClass))

typedef struct _ChartsSeries ChartsSeries;
typedef struct _ChartsSeriesClass ChartsSeriesClass;

CHARTS_AVAILABLE_IN_ALL
GType       charts_series_get_type  (void) G_GNUC_CONST;
CHARTS_AVAILABLE_IN_ALL
GListModel *charts_series_get_model (ChartsSeries *self);
CHARTS_AVAILABLE_IN_ALL
void        charts_series_set_model (ChartsSeries *self,
                                     GListModel   *model);
CHARTS_AVAILABLE_IN_ALL
const char *charts_series_get_title (ChartsSeries *self);
CHARTS_AVAILABLE_IN_ALL
void        charts_series_set_title (ChartsSeries *self,
                                     const char   *title);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ChartsSeries, g_object_unref)

G_END_DECLS
